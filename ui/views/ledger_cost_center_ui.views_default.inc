<?php

/**
 * @file
 * Views for the default Ledger Cost Center UI.
 */

/**
 * Implements hook_views_default_views().
 */

function ledger_cost_center_ui_views_default_views() {

  $files = file_scan_directory(drupal_get_path('module', 'ledger_cost_center_ui'). '/views', '/\.views.inc/');

  foreach ($files as $filepath => $file) {

    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }

  return $views;
}
