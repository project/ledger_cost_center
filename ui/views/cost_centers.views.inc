<?php

$view = new view();
$view->name = 'ledger_cost_centers';
$view->description = '';
$view->tag = 'ledger';
$view->base_table = 'ledger_cost_center';
$view->human_name = 'Ledger Cost Centers';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Ledger Cost Centers';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '25';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'name' => 'name',
  'description' => 'description',
  'cost_center_ops' => 'cost_center_ops',
);
$handler->display->display_options['style_options']['default'] = 'name';
$handler->display->display_options['style_options']['info'] = array(
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'description' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'cost_center_ops' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'There is no cost centers created yet.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Field: Ledger Cost center: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'ledger_cost_center';
$handler->display->display_options['fields']['name']['field'] = 'name';
/* Field: Ledger Cost center: Description */
$handler->display->display_options['fields']['description']['id'] = 'description';
$handler->display->display_options['fields']['description']['table'] = 'ledger_cost_center';
$handler->display->display_options['fields']['description']['field'] = 'description';
/* Field: Ledger Cost center: Operations */
$handler->display->display_options['fields']['cost_center_ops']['id'] = 'cost_center_ops';
$handler->display->display_options['fields']['cost_center_ops']['table'] = 'ledger_cost_center';
$handler->display->display_options['fields']['cost_center_ops']['field'] = 'cost_center_ops';

/* Display: All Cost Centers (page) */
$handler = $view->new_display('page', 'All Cost Centers (page)', 'page');
$handler->display->display_options['path'] = 'admin/ledger/cost-centers';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Cost centers';
$handler->display->display_options['menu']['weight'] = '10';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Cost Centers by book (page) */
$handler = $view->new_display('page', 'Cost Centers by book (page)', 'page_1');
$handler->display->display_options['enabled'] = FALSE;
$handler->display->display_options['path'] = 'ledger/book/%/cost-centers';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Cost centers';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['ledger_cost_centers'] = array(
  t('Master'),
  t('Ledger Cost Centers'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('There is no cost centers created yet.'),
  t('Name'),
  t('Description'),
  t('Operations'),
  t('All Cost Centers (page)'),
  t('Cost Centers by book (page)'),
);

return $view;
