<?php

$view = new view();
$view->name = 'ledger_cost_center';
$view->description = '';
$view->tag = 'ledger';
$view->base_table = 'ledger_transaction';
$view->human_name = 'Ledger Cost Center';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Cost Center';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'view all ledger_cost_center';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'tid' => 'tid',
  'name' => 'name',
  'ledger_value_1' => 'ledger_value_1',
  'ledger_value' => 'ledger_value',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'tid' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'ledger_value_1' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'ledger_value' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'No transactions found for this cost center.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: Transaction: Account entry */
$handler->display->display_options['relationships']['ledger_account_entry']['id'] = 'ledger_account_entry';
$handler->display->display_options['relationships']['ledger_account_entry']['table'] = 'ledger_transaction';
$handler->display->display_options['relationships']['ledger_account_entry']['field'] = 'ledger_account_entry';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['ledger_cost_center_target_id']['id'] = 'ledger_cost_center_target_id';
$handler->display->display_options['relationships']['ledger_cost_center_target_id']['table'] = 'field_data_ledger_cost_center';
$handler->display->display_options['relationships']['ledger_cost_center_target_id']['field'] = 'ledger_cost_center_target_id';
$handler->display->display_options['relationships']['ledger_cost_center_target_id']['relationship'] = 'ledger_account_entry';
$handler->display->display_options['relationships']['ledger_cost_center_target_id']['label'] = 'Ledger Cost center';
/* Relationship: Account entry: Account aid */
$handler->display->display_options['relationships']['account']['id'] = 'account';
$handler->display->display_options['relationships']['account']['table'] = 'ledger_account_entry';
$handler->display->display_options['relationships']['account']['field'] = 'account';
$handler->display->display_options['relationships']['account']['relationship'] = 'ledger_account_entry';
/* Field: Transaction: Transaction ID */
$handler->display->display_options['fields']['tid']['id'] = 'tid';
$handler->display->display_options['fields']['tid']['table'] = 'ledger_transaction';
$handler->display->display_options['fields']['tid']['field'] = 'tid';
/* Contextual filter: Ledger Cost center: Cost center ID */
$handler->display->display_options['arguments']['ccid']['id'] = 'ccid';
$handler->display->display_options['arguments']['ccid']['table'] = 'ledger_cost_center';
$handler->display->display_options['arguments']['ccid']['field'] = 'ccid';
$handler->display->display_options['arguments']['ccid']['relationship'] = 'ledger_cost_center_target_id';
$handler->display->display_options['arguments']['ccid']['default_action'] = 'not found';
$handler->display->display_options['arguments']['ccid']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['ccid']['title'] = 'Transactions in cost center: %1';
$handler->display->display_options['arguments']['ccid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['ccid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['ccid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['ccid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['ccid']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['ccid']['validate']['type'] = 'ledger_cost_center';
$handler->display->display_options['arguments']['ccid']['validate_options']['access'] = TRUE;
$handler->display->display_options['arguments']['ccid']['validate']['fail'] = 'access denied';

/* Display: Cost center (page) */
$handler = $view->new_display('page', 'Cost center (page)', 'page');
$handler->display->display_options['defaults']['query'] = FALSE;
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Transaction: Timestamp */
$handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['table'] = 'ledger_transaction';
$handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['date_format'] = 'short';
$handler->display->display_options['fields']['timestamp']['second_date_format'] = 'long';
/* Field: Transaction: Description */
$handler->display->display_options['fields']['description']['id'] = 'description';
$handler->display->display_options['fields']['description']['table'] = 'ledger_transaction';
$handler->display->display_options['fields']['description']['field'] = 'description';
/* Field: Account entry: Value */
$handler->display->display_options['fields']['ledger_value_1']['id'] = 'ledger_value_1';
$handler->display->display_options['fields']['ledger_value_1']['table'] = 'field_data_ledger_value';
$handler->display->display_options['fields']['ledger_value_1']['field'] = 'ledger_value';
$handler->display->display_options['fields']['ledger_value_1']['relationship'] = 'ledger_account_entry';
$handler->display->display_options['fields']['ledger_value_1']['label'] = 'Debit';
$handler->display->display_options['fields']['ledger_value_1']['type'] = 'fraction_decimal';
$handler->display->display_options['fields']['ledger_value_1']['settings'] = array(
  'precision' => '2',
  'auto_precision' => 1,
);
$handler->display->display_options['fields']['ledger_value_1']['condition'] = 'increase';
/* Field: Account entry: Value */
$handler->display->display_options['fields']['ledger_value']['id'] = 'ledger_value';
$handler->display->display_options['fields']['ledger_value']['table'] = 'field_data_ledger_value';
$handler->display->display_options['fields']['ledger_value']['field'] = 'ledger_value';
$handler->display->display_options['fields']['ledger_value']['relationship'] = 'ledger_account_entry';
$handler->display->display_options['fields']['ledger_value']['label'] = 'Credit';
$handler->display->display_options['fields']['ledger_value']['type'] = 'fraction_decimal';
$handler->display->display_options['fields']['ledger_value']['settings'] = array(
  'precision' => '2',
  'auto_precision' => 1,
);
$handler->display->display_options['fields']['ledger_value']['condition'] = 'decrease';
/* Field: Account: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'ledger_account';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'account';
$handler->display->display_options['fields']['name']['label'] = 'Account';
$handler->display->display_options['fields']['name']['link_to_account'] = 'view';
$handler->display->display_options['path'] = 'ledger/cost-center/%';
$translatables['ledger_cost_center'] = array(
  t('Master'),
  t('Cost Center'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('No transactions found for this cost center.'),
  t('Account entry'),
  t('Ledger Cost center'),
  t('Account'),
  t('Transaction ID'),
  t('.'),
  t(','),
  t('All'),
  t('Transactions in cost center: %1'),
  t('Cost center (page)'),
  t('Timestamp'),
  t('Description'),
  t('Debit'),
  t('Credit'),
);

return $view;
