<?php
/**
 * @file
 * Ledger Cost center
 */

/**
 * Implements hook_permission().
 */
function ledger_cost_center_permission() {

  // Add granular entity permissions via ledger's helper function.
  return ledger_entity_access_permissions('ledger_cost_center');
}

/**
 * Implements hook_entity_info().
 */
function ledger_cost_center_entity_info() {
  $entities['ledger_cost_center'] = array(
    'label' => t('Ledger Cost center'),
    'label plural' => t('Cost centers'),
    'entity class' => 'LedgerCostCenter',
    'controller class' => 'LedgerCostCenterController',
    'base table' => 'ledger_cost_center',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'ccid',
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'module' => 'ledger_cost_center',
    'access callback' => 'ledger_cost_center_access',
    'bundles' => array(
      'ledger_cost_center' => array(
        'label' => t('Cost center', array(), array('context' => 'a ledger cost center')),
      ),
    ),
  );
  return $entities;
}


/**
 * The class for LedgerCostcenter entities.
 */
class LedgerCostcenter extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'ledger_cost_center');
  }

  protected function defaultLabel() {
    return t('@name', array('@name' => $this->name));
  }

  protected function defaultUri() {
    return array('path' => 'ledger/ledger_cost_center/' . $this->ccid);
  }
}

/**
 * The controller for LedgerCostcenter entities
 */
class LedgerCostCenterController extends EntityAPIController {

  /**
   * Create an cost center.
   *
   * We first set up the values that are specific to our ledger_cost_center
   * schema but then also go through the EntityAPIController function.
   *
   * @param array $values
   *   Values passed in.
   *
   * @return object
   *   An cost center object with all default fields initialized.
   */
  public function create(array $values = array()) {

    // Add values that are specific to our cost center.
    $values += array(
      'ccid' => '',
      'name' => '',
      'description' => '',
    );

    $cost_center = parent::create($values);
    return $cost_center;
  }
}


/**
 * Access callback for ledger entities.
 *
 * @param string $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'.
 * @param object $entity
 *   Optionally a specific ledger entity to check.
 * @param object $account
 *   The user to check for. Leave it to NULL to check for the global user.
 *
 * @return bool
 *   Whether access is allowed or not.
 */
function ledger_cost_center_access($op, $entity = NULL, $account = NULL) {
  return ledger_entity_access($op, $entity, $account, 'ledger_cost_center');
}

/**
 * Returns a title for the cost center.
 *
 * @param object $cost_center
 *   A ledger cost center object.
 *
 * @return string
 *   A string that represents the cost center name.
 */
function ledger_cost_center_title($cost_center) {
  return $cost_center->name;
}

/**
 * Returns an initialized cost center object.
 *
 * @return object
 *   An cost center object with all default fields initialized.
 */
function ledger_cost_center_new($values = array()) {
  return entity_get_controller('ledger_cost_center')->create($values);
}

/**
 * Loads an cost center by ccid.
 *
 * @param int $ccid
 *   The ID of the cost center to load.
 */
function ledger_cost_center_load($ccid, $reset = FALSE) {
  $cost_centers = ledger_cost_center_load_multiple(array($ccid), array(), $reset);
  return $cost_centers ? reset($cost_centers) : FALSE;
}

/**
 * Loads multiple cost centers by ID or based on a set of matching conditions.
 *
 * @see entity_load()
 *
 * @param array $ccids
 *   An array of cost center IDs.
 * @param array $conditions
 *   An array of conditions on the {ledger_cost_center} table in the form
 *     'field' => $value.
 * @param bool $reset
 *   Whether to reset the internal cost center loading cache.
 *
 * @return array
 *   An array of cost center objects indexed by id.
 */
function ledger_cost_center_load_multiple($ccids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('ledger_cost_center', $ccids, $conditions, $reset);
}

/**
 * Saves an cost center to the database.
 *
 * @param object $cost_center
 *   The cost center object.
 */
function ledger_cost_center_save($cost_center) {
  return entity_get_controller('ledger_cost_center')->save($cost_center);
}

/**
 * Deletes an cost center by ID.
 *
 * @param int $ccid
 *   The ID of the cost center to delete.
 *
 * @return bool
 *   TRUE on success, FALSE otherwise.
 */
function ledger_cost_center_delete($ccid) {
  return ledger_cost_center_delete_multiple(array($ccid));
}

/**
 * Deletes multiple cost centers by ID.
 *
 * @param array $ccids
 *   An array of cost center IDs to delete.
 *
 * @return bool
 *   TRUE on success, FALSE otherwise.
 */
function ledger_cost_center_delete_multiple($ccids) {
  return entity_get_controller('ledger_cost_center')->delete($ccids);
}

/**
 * Create ledger_balance field.
 */
function ledger_cost_center_configure_cost_center_field() {
  // Load field and instance information about existing fields.
  $field_name = 'ledger_cost_center';
  $field = field_info_field($field_name);

  // If the field doesn't exist, create it.
  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'active' => 1,
      'cardinality' => 1,
      'deleted' => 0,
      'entity_types' => array(),
      'foreign keys' => array(
        'ledger_account_entry' => array(
          'columns' => array(
            'target_id' => 'ccid',
          ),
          'table' => 'ledger_cost_center',
        ),
      ),
      'indexes' => array(
        'target_id' => array(
          0 => 'target_id',
        ),
      ),
      'locked' => 0,
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'type' => 'none',
          ),
          'target_bundles' => array(),
        ),
        'target_type' => 'ledger_cost_center',
      ),
      'translatable' => 0,
      'type' => 'entityreference',
    );

    $field = field_create_field($field);
  }

  // Return the field name.
  return $field_name;
}

/**
 * Add Balance field to a bundle.
 */
function ledger_cost_center_configure_cost_center_field_instance($type, $bundle) {

  // Create the ledger_balance field, if necessary.
  $field_name = ledger_cost_center_configure_cost_center_field();

  // Look up existing instance information.
  $instance = field_info_instance($type, $field_name, $bundle);

  // If the instance doesn't exist, create it.
  if (empty($instance)) {
    $instance = array(
      'bundle' => 'ledger_account_entry',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'link' => FALSE,
          ),
          'type' => 'entityreference_label',
          'weight' => 101,
        ),
      ),
      'entity_type' => 'ledger_account_entry',
      'field_name' => 'ledger_cost_center',
      'label' => 'Cost center',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => 101,
      ),
    );

    // Create the field instance.
    field_create_instance($instance);
  }
}

/**
 * Implements hook_views_api().
 */
function ledger_cost_center_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'ledger_cost_center') . '/views',
  );
}

/**
 * Returns a list of cost centers to use in a form select element.
 */
function ledger_cost_center_select_options() {

  // Give it a 0 value to start.
  $options[0] = '- Select -';

  // Get all cost centers.
  $cost_centers = entity_load('ledger_cost_center', FALSE, FALSE, TRUE);

  // Loop through the cost centers.
  foreach ($cost_centers as $cost_center) {
    // Add the cost center to the options.
    $options[$cost_center->ccid] = $cost_center->name;
  }

  return $options;
}
