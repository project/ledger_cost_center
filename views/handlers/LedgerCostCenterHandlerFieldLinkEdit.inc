<?php
/**
 * @file
 * Field handler to present a cost_center edit link.
 */

class LedgerCostCenterHandlerFieldLinkEdit extends LedgerCostCenterHandlerFieldLink {

  /**
   * Function that render the field.
   */
  public function render($values) {

    // Ensure the user has access to edit this order.
    $cost_center = ledger_cost_center_load($this->get_value($values, 'ccid'));
    if (!ledger_cost_center_access('update', $cost_center)) {
      return;
    }

    // Assemble the link options.
    $options = array();
    if (!empty($this->options['destination'])) {
      $options['query']['destination'] = current_path();
    }

    // Load the link text.
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');

    // Return the text as a link to the cost center edit page.
    return l($text, 'ledger/cost-center/' . $cost_center->ccid . '/edit', $options);
  }
}
