<?php
/**
 * @file
 * Field handler to present a link to a cost_center.
 */

class LedgerCostCenterHandlerFieldLink extends views_handler_field {

  /**
   * Construct the handler.
   */
  public function construct() {

    // Inherit the parent's construction.
    parent::construct();

    // Add the cost_center id as an additional field to load to
    // ensure that it is available in this handler.
    $this->additional_fields['ccid'] = 'ccid';
  }

  /**
   * Define the options.
   */
  public function optionDefinition() {

    // Inherit the parent's options.
    $options = parent::optionDefinition();

    // Allow users to toggle the ?destination parameter.
    $options['destination'] = array(
      'default' => 1,
      'translatable' => FALSE,
    );

    // Provide a text field so users can change the text of the link.
    $options['text'] = array(
      'default' => '',
      'translatable' => TRUE,
    );

    return $options;
  }

  /**
   * Define the options form.
   */
  public function optionsForm(&$form, &$form_state) {

    // Inherit the parent's options form.
    parent::optionsForm($form, $form_state);

    // Allow users to toggle the ?destination parameter.
    $form['destination'] = array(
      '#type' => 'checkbox',
      '#title' => t('Destination'),
      '#description' => t('Add a ?destination parameter to the link pointing back to the current page.'),
      '#default_value' => $this->options['destination'],
    );

    // Provide a text field so users can change the text of the link.
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  /**
   * Add fields to the query.
   */
  public function query() {

    // Ensure the main table for this field is included.
    $this->ensure_my_table();

    // Include additional fields.
    $this->add_additional_fields();
  }

  /**
   * Function that render the field.
   */
  public function render($values) {

    // Assemble the link options.
    $options = array();
    if (!empty($this->options['destination'])) {
      $options['query']['destination'] = current_path();
    }

    // Load the link text.
    $text = !empty($this->options['text']) ? $this->options['text'] : t('view');

    // Load the cost_center id value.
    $ccid = $this->get_value($values, 'ccid');

    // Return the text as a link to the cost_center page.
    return l($text, 'ledger/cost-center/' . $ccid, $options);
  }
}
