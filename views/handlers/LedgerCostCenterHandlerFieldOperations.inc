<?php
/**
 * @file
 * Field handler to present a list of links to cost center operations.
 */

class LedgerCostCenterHandlerFieldOperations extends LedgerCostCenterHandlerFieldLink {

  /**
   * Alter the field options form.
   */
  public function optionsForm(&$form, &$form_state) {

    // Inherit the parent form.
    parent::optionsForm($form, $form_state);

    // Unset the 'text' field that was defined by the parent class.
    unset($form['text']);
  }

  /**
   * Function that render the field.
   */
  public function render($values) {

    // Ensure the user has access to edit this cost center.
    $cost_center = ledger_cost_center_load($this->get_value($values, 'ccid'));
    if (!ledger_cost_center_access('update', $cost_center)) {
      return;
    }

    // Assemble the link options.
    $options = array();
    if (!empty($this->options['destination'])) {
      $options['query']['destination'] = current_path();
    }

    // Define the operations to include.
    $ops = array(
      l(t('View'), 'ledger/cost-center/' . $cost_center->ccid . '/view', $options),
      l(t('Edit'), 'ledger/cost-center/' . $cost_center->ccid . '/edit', $options),
      l(t('Delete'), 'ledger/cost-center/' . $cost_center->ccid . '/delete', $options),
    );

    // Assemble the list of operations.
    return implode(' | ', $ops);
  }
}
