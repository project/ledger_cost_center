<?php

/**
 * @file
 * Contains the 'cost_center' argument validator plugin.
 */

/**
 * Validate whether an argument is an acceptable cost center.
 */
class ledger_cost_center_plugin_argument_validate_cost_center extends views_plugin_argument_validate {
  function option_definition() {
    $options = parent::option_definition();
    $options['access'] = array('default' => FALSE, 'bool' => TRUE);
    $options['access_op'] = array('default' => 'view');
    $options['ccid_type'] = array('default' => 'ccid');

    return $options;
  }

  function options_form(&$form, &$form_state) {

    $form['access'] = array(
      '#type' => 'checkbox',
      '#title' => t('Validate user has access to the Cost Center'),
      '#default_value' => $this->options['access'],
    );
    $form['access_op'] = array(
      '#type' => 'radios',
      '#title' => t('Access operation to check'),
      '#options' => array('view' => t('View'), 'edit' => t('Edit')),
      '#default_value' => $this->options['access_op'],
      '#dependency' => array('edit-options-validate-options-cost-center-access' => array(TRUE)),
    );

    $form['ccid_type'] = array(
      '#type' => 'select',
      '#title' => t('Filter value format'),
      '#options' => array(
        'ccid' => t('Cost center ID'),
        'ccids' => t('Cost center IDs separated by , or +'),
      ),
      '#default_value' => $this->options['ccid_type'],
    );
  }

  function validate_argument($argument) {

    switch ($this->options['ccid_type']) {
      case 'ccid':
        if (!is_numeric($argument)) {
          return FALSE;
        }
        $cost_center = ledger_cost_center_load($argument);
        if (!$cost_center) {
          return FALSE;
        }

        if (!empty($this->options['access'])) {
          if (!ledger_cost_center_access($this->options['access_op'], $cost_center)) {
            return FALSE;
          }
        }

        // Save the title() handlers some work.
        $this->argument->validated_title = check_plain($cost_center->name);

        // Valid!
        return TRUE;

      case 'ccids':
        $ccids = new stdClass();
        $ccids->value = array($argument);
        $ccids = views_break_phrase($argument, $ccids);
        if ($ccids->value == array(-1)) {
          return FALSE;
        }

        $test = drupal_map_assoc($ccids->value);
        $titles = array();

        $result = db_query("SELECT * FROM {ledger_cost_center} WHERE ccid IN (:ccids)", array(':ccids' =>  $ccids->value));
        foreach ($result as $cost_center) {

          if (!empty($this->options['access'])) {
            if (!ledger_cost_center_access($this->options['access_op'], $cost_center)) {
              return FALSE;
            }
          }

          $titles[] = check_plain($cost_center->name);
          unset($test[$cost_center->ccid]);
        }

        $this->argument->validated_title = implode($ccids->operator == 'or' ? ' + ' : ', ', $titles);
        // If this is not empty, we did not find a ccid.
        return empty($test);
    }
  }
}
