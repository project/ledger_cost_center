<?php

/**
 * @file
 * Ledger Cost Center Views data.
 */

/**
 * Implements hook_views_plugins().
 */
function ledger_cost_center_views_plugins() {
  return array(
    'argument validator' => array(
      'ledger_cost_center' => array(
        'title' => t('Ledger cost center'),
        'handler' => 'ledger_cost_center_plugin_argument_validate_cost_center',
      ),
    ),
  );
}

/**
 * Implements hook_views_data_alter().
 */
function ledger_cost_center_views_data_alter(&$data) {

  // Add a field for transaction view link.
  $data['ledger_cost_center']['view_cost_center'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a simple link to the cost center.'),
      'handler' => 'LedgerCostCenterHandlerFieldLink',
    ),
  );

  // Add a field for cost center edit link.
  $data['ledger_cost_center']['edit_cost_center'] = array(
    'field' => array(
      'title' => t('Edit link'),
      'help' => t('Provide a simple link to edit the cost center.'),
      'handler' => 'LedgerCostCenterHandlerFieldLinkEdit',
    ),
  );

  // Add a field for cost center delete link.
  $data['ledger_cost_center']['delete_cost_center'] = array(
    'field' => array(
      'title' => t('Delete link'),
      'help' => t('Provide a simple link to delete the cost center.'),
      'handler' => 'LedgerCostCenterHandlerFieldLinkDelete',
    ),
  );

  // Add a field for common cost center operations.
  // This is just a combination of Edit, and Delete fields above.
  $data['ledger_cost_center']['cost_center_ops'] = array(
    'field' => array(
      'title' => t('Operations'),
      'help' => t('Links to various cost center operations.'),
      'handler' => 'LedgerCostCenterHandlerFieldOperations',
    ),
  );
}
