<?php

/**
 * @file
 * Ledger Cost center property info
 */

/**
 * Implements hook_entity_property_info().
 */
function ledger_cost_center_entity_property_info() {
  $info = array();

  // Add meta-data about the cost center properties.
  $cost_center = &$info['ledger_cost_center']['properties'];

  $cost_center['ccid'] = array(
    'type' => 'integer',
    'label' => t('Cost center ID'),
    'description' => t('The unique ID of the ledger cost center.'),
    'setter permission' => 'administer ledger',
    'schema field' => 'ccid',
  );
  $cost_center['name'] = array(
    'label' => t('Name'),
    'description' => t('The name of the cost center.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer ledger',
    'required' => TRUE,
    'schema field' => 'name',
  );
  $cost_center['description'] = array(
    'label' => t('Description'),
    'description' => t('A brief description of the cost center.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer ledger',
    'required' => FALSE,
    'schema field' => 'description',
  );

  return $info;
}
